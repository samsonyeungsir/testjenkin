FROM node:18-bullseye

# ARG -v /etc/passwd:/etc/passwd
# RUN chmod g=u /etc/passwd
# Install ionic
RUN npm install -g @ionic/cli

WORKDIR /usr/src
RUN git clone https://gitlab.com/samsonyeungsir/testjenkin.git .
RUN npm install
RUN ionic capacitor build android --no-open
# RUN npm i gradle --save-dev