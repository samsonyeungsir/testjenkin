FROM node:18-bullseye

RUN npm install android-sdk
RUN npm i jdk
RUN npm i gradle
RUN gradle --version
RUN java --version