import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.vanvideo.taxieco.www',
  appName: 'testjenkin',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
